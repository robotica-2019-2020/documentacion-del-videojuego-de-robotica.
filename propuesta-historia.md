# PROPUESTA DE HISTORIA PARA EL VIDEOJUEGO
#### Juan Antonio Oliver

## Índice de capítulos

   1. Prólogo
   1. Presentación del personaje
   1. La Peste Negra
   1. El ébola

## Prólogo

Aquí el jugador solo sería un mero espectador. Se le mostrarían unas imágenes del laboratorio donde se crea la vacuna.

## Capítulo 1- Presentación del personaje

Conocemos al personaje que manejaremos. Este personaje será agente de una agencia de inteligencia y cuando llega a su trabajo un día cualquiera llega información filtrada del laboratorio. Los agentes intentan primero averiguar que tipo de arma biológica es. Finalmente llegan a la conclusión de que el agente biológico es una mezcla de enfermedades devastadoras: peste negra, ébola, plaga de Atenas y gripe española. El director de la agencia de inteligencia le encarga la misión a nuestro agente de viajar a las cuatro épocas de grandes pandemias de estas enfermedades para evitar que las cepas sean llevadas al presente para la fabricación del arma.

## Capítulo 2- La Peste Negra.

A través de una máquina del tiempo experimental, nuestro agente llega a 1348, en plena epidemia de peste bubónica en Europa, en la ciudad de Florencia, una de las más afectadas. En el camino no consigue descubrir a los agentes rivales y al final debe volver a casa sin éxito tras superar X retos.

## Capítulo 3- El ébola

El agente llega entonces a la Sierra Leona de 2014, en medio de la epidemia de ébola. Explorando la zona para hallar a los agentes enemigos, deberá enfrentarse a X retos.
Tampoco consigue nada, pero cuando vuelve se encuentra mal; había contraído peste negra en su primer viaje. Sobrevive, pero la enfermedad se extiende por la agencia y contagia a varios agentes, poniendo en cuarentena al edificio e inhabilitando a nuestro agente de poder pasar por la agencia entre viaje y viaje. Eso le hace pasar por épocas históricas que no forman parte de la misión :

   - Roma 70 d.C
   - Dresde 1945 d.C
   - Lepanto 1572 d.C
   - Altamira 3000 a.C

## Capítulo 4- La plaga de Atenas

Al final llega a su destino: la Atenas del 430 a.C para intentar detener a los agentes que robaron la cepa de la plaga. Sin embargo, se ve envuelto en una cruenta batalla entre atenienses y espartanos, y aquí tendrá una decisión importante: salvar a un comandante ateniense o dejarlo morir. Esto divide la historia en dos partes.

### Capítulo 5A- Cambio decisivo

Vuelve finalmente al presente tras salvar la vida del comandante ateniense pero descubre un futuro distinto: según la historia ese comandante había fallecido en esa batalla, y su desciendente asesinó al padre de Miguel de Cervantes, por lo que el Quijote nunca se escribió. Debe viajar a 1560 para detener al asesino de Rodrigo de Cervantes y salvar la historia.

### Capítulo 5B- Escapar de la prisión

Tras dejar morir al comandante ateniense, la justicia ateniense lo encierra en prisión. Deberá ingeniárselas para escapar exitosamente. Deberá tener cuidado pues se cruzará con Sócrates, y podría matarlo cambiando asi para siempre la historia. Sin embargo, Sócrates sobrevive y el agente vuelve al presente para encarar su última misión.

## Capítulo 6- La gripe española

EL agente viaja a Texas en 1918 en su última oportunidad de salvar el mundo. Allí, descubre a los agentes enemigos y los detiene. Se los lleva a la fuerza a la Alemania de 1943 y los entrega como judíos al régimen nazi para deshacerse de ellos.

## Capítulo 7- Final

Una vez ya en el presente, descubre que la misión ha sido exitosa, pues la vacuna nunca se fabricó. Pero tuvo que pagar un coste, pues muchos de sus compañeros mueren en el brote de Peste Negra en el edificio.
